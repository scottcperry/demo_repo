{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Building Automated MLops Workflows with Amazon SageMaker and the AWS Step Functions Data Science SDK\n",
    "\n",
    "[Amazon SageMaker](https://aws.amazon.com/sagemaker/) is the end to end machine learning platform offered by AWS. Targeted toward ML engineers, data scientists, and developers alike, SageMaker offers a wide variety of features and services that encompass all aspects of the machine learning process such as data labelling, data pre-processing, model training/tuning, model hosting, model monitoring, and debugging.\n",
    "\n",
    "With Amazon SageMaker Processing, you can leverage a simplified, managed experience to run data pre- or post-processing and model evaluation workloads on the Amazon SageMaker platform. SageMaker Processing provides managed containers for both Scikit-Learn and Apache Spark data processing jobs, and also allows users to leverage virtually any available tool/framework by providing bring-your-own-container capabilities.\n",
    "\n",
    "The Step Functions SDK is an open source library that allows data scientists to easily create and execute machine learning workflows using AWS Step Functions and Amazon SageMaker. For more information, please see the following resources:\n",
    "* [AWS Step Functions](https://aws.amazon.com/step-functions/)\n",
    "* [AWS Step Functions Developer Guide](https://docs.aws.amazon.com/step-functions/latest/dg/welcome.html)\n",
    "* [AWS Step Functions Data Science SDK](https://aws-step-functions-data-science-sdk.readthedocs.io)\n",
    "\n",
    "This notebook describes how to use the AWS Step Functions Data Science SDK to build a Workflow that will:\n",
    "* perform data pre-processing using SageMaker Processing\n",
    "* schedule repeated execution of the SageMaker Processing job\n",
    "\n",
    "The notebook also includes basic yet functional data pre-processing code that leverages the [Census-Income KDD Dataset](https://archive.ics.uci.edu/ml/datasets/Census-Income+%28KDD%29).\n",
    "\n",
    "**Please note** that the focus of this notebook is the creation and execution of the ML Workflow, and not the specific techniques used in the sample pre-processing script.\n",
    "\n",
    "The Workflow-specific concepts outlined in this notebook are intended to serve as building blocks that can be applied to any number of ML use cases, and can easily be extended to accommodate more complex Workflows, as required."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Initial Setup"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# NOTE: Please specify a unique label, such as your first initial and surname, below\n",
    "job_label = \"JSMITH\" # ex: JSMITH for John Smith"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import the latest sagemaker and stepfunctions SDKs\n",
    "import sys\n",
    "!{sys.executable} -m pip install stepfunctions==2.0.0rc1\n",
    "!{sys.executable} -m pip show sagemaker stepfunctions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Import the Required Modules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import io\n",
    "import logging\n",
    "import os\n",
    "import random\n",
    "import time\n",
    "import uuid\n",
    "\n",
    "import boto3\n",
    "import stepfunctions\n",
    "from stepfunctions import steps\n",
    "from stepfunctions.inputs import ExecutionInput\n",
    "from stepfunctions.steps import (\n",
    "    Chain,\n",
    "    ChoiceRule,\n",
    "    ModelStep,\n",
    "    ProcessingStep,\n",
    "    TrainingStep,\n",
    "    TransformStep,\n",
    ")\n",
    "from stepfunctions.template import TrainingPipeline\n",
    "from stepfunctions.template.utils import replace_parameters_with_jsonpath\n",
    "from stepfunctions.workflow import Workflow\n",
    "\n",
    "import sagemaker\n",
    "from sagemaker import get_execution_role\n",
    "from sagemaker.amazon.amazon_estimator import get_image_uri\n",
    "from sagemaker.processing import ProcessingInput, ProcessingOutput\n",
    "from sagemaker.s3 import S3Uploader\n",
    "from sagemaker.sklearn.processing import SKLearnProcessor\n",
    "\n",
    "# SageMaker Session\n",
    "sagemaker_session = sagemaker.Session()\n",
    "region = sagemaker_session.boto_region_name\n",
    "\n",
    "# SageMaker Execution Role\n",
    "# You can use sagemaker.get_execution_role() if running inside sagemaker's notebook instance\n",
    "role = get_execution_role()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"SageMaker execution role:\\n  %s\" % role)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<br/>\n",
    "\n",
    "Next, we'll create and/or modify IAM roles for both Step Functions and SageMaker. The IAM roles grant the services permissions within your AWS environment.\n",
    "\n",
    "## Add permissions to your notebook role in IAM\n",
    "\n",
    "The IAM role assumed by your notebook requires permission to create and run workflows in AWS Step Functions. If this notebook is running on a SageMaker notebook instance, do the following to provide IAM permissions to the notebook:\n",
    "\n",
    "1. Open the Amazon [SageMaker console](https://console.aws.amazon.com/sagemaker/). \n",
    "2. Select **Notebook instances** and choose the name of your notebook instance.\n",
    "3. Under **Permissions and encryption** select the role ARN to view the role on the IAM console.\n",
    "4. Copy and save the IAM role ARN for later use. \n",
    "5. Choose **Attach policies** and search for `AWSStepFunctionsFullAccess`.\n",
    "6. Select the check box next to `AWSStepFunctionsFullAccess` and choose **Attach policy**.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, let's create an execution role in IAM for Step Functions. \n",
    "\n",
    "## Create an Execution Role for Step Functions\n",
    "\n",
    "Your Step Functions workflow requires an IAM role to interact with other services in your AWS environment. \n",
    "\n",
    "1. Go to the [IAM console](https://console.aws.amazon.com/iam/).\n",
    "2. Select **Roles** and then **Create role**.\n",
    "3. Under **Choose the service that will use this role** select **Step Functions**.\n",
    "4. Choose **Next** until you can enter a **Role name**.\n",
    "5. Enter a name such as `StepFunctionsWorkflowExecutionRole` and then select **Create role**.\n",
    "\n",
    "Next, attach a AWS Managed IAM policy to the role you created as per below steps.\n",
    "\n",
    "1. Go to the [IAM console](https://console.aws.amazon.com/iam/).\n",
    "2. Select **Roles**\n",
    "3. Search for `StepFunctionsWorkflowExecutionRole` IAM Role\n",
    "4. Under the **Permissions** tab, click **Attach policies** and then search for `CloudWatchEventsFullAccess` IAM Policy managed by AWS.\n",
    "5. Click on `Attach Policy`\n",
    "\n",
    "\n",
    "Next, create and attach another new policy to the role you created. As a best practice, the following steps will attach a policy that only provides access to the specific resources and actions needed for this solution.\n",
    "\n",
    "1. Under the **Permissions** tab, click **Attach policies** and then **Create policy**.\n",
    "2. Enter the following in the **JSON** tab:\n",
    "\n",
    "```json\n",
    "{\n",
    "    \"Version\": \"2012-10-17\",\n",
    "    \"Statement\": [\n",
    "        {\n",
    "            \"Sid\": \"VisualEditor0\",\n",
    "            \"Effect\": \"Allow\",\n",
    "            \"Action\": [\n",
    "                \"events:PutTargets\",\n",
    "                \"events:DescribeRule\",\n",
    "                \"events:PutRule\"\n",
    "            ],\n",
    "            \"Resource\": [\n",
    "                \"arn:aws:events:*:*:rule/StepFunctionsGetEventsForSageMakerTrainingJobsRule\",\n",
    "                \"arn:aws:events:*:*:rule/StepFunctionsGetEventsForSageMakerTransformJobsRule\",\n",
    "                \"arn:aws:events:*:*:rule/StepFunctionsGetEventsForSageMakerTuningJobsRule\",\n",
    "                \"arn:aws:events:*:*:rule/StepFunctionsGetEventsForECSTaskRule\",\n",
    "                \"arn:aws:events:*:*:rule/StepFunctionsGetEventsForBatchJobsRule\"\n",
    "            ]\n",
    "        },\n",
    "        {\n",
    "            \"Sid\": \"VisualEditor1\",\n",
    "            \"Effect\": \"Allow\",\n",
    "            \"Action\": \"iam:PassRole\",\n",
    "            \"Resource\": \"NOTEBOOK_ROLE_ARN\",\n",
    "            \"Condition\": {\n",
    "                \"StringEquals\": {\n",
    "                    \"iam:PassedToService\": \"sagemaker.amazonaws.com\"\n",
    "                }\n",
    "            }\n",
    "        },\n",
    "        {\n",
    "            \"Sid\": \"VisualEditor2\",\n",
    "            \"Effect\": \"Allow\",\n",
    "            \"Action\": [\n",
    "                \"batch:DescribeJobs\",\n",
    "                \"batch:SubmitJob\",\n",
    "                \"batch:TerminateJob\",\n",
    "                \"dynamodb:DeleteItem\",\n",
    "                \"dynamodb:GetItem\",\n",
    "                \"dynamodb:PutItem\",\n",
    "                \"dynamodb:UpdateItem\",\n",
    "                \"ecs:DescribeTasks\",\n",
    "                \"ecs:RunTask\",\n",
    "                \"ecs:StopTask\",\n",
    "                \"glue:BatchStopJobRun\",\n",
    "                \"glue:GetJobRun\",\n",
    "                \"glue:GetJobRuns\",\n",
    "                \"glue:StartJobRun\",\n",
    "                \"lambda:InvokeFunction\",\n",
    "                \"sagemaker:CreateEndpoint\",\n",
    "                \"sagemaker:CreateEndpointConfig\",\n",
    "                \"sagemaker:CreateHyperParameterTuningJob\",\n",
    "                \"sagemaker:CreateModel\",\n",
    "                \"sagemaker:CreateProcessingJob\",\n",
    "                \"sagemaker:CreateTrainingJob\",\n",
    "                \"sagemaker:CreateTransformJob\",\n",
    "                \"sagemaker:DeleteEndpoint\",\n",
    "                \"sagemaker:DeleteEndpointConfig\",\n",
    "                \"sagemaker:DescribeHyperParameterTuningJob\",\n",
    "                \"sagemaker:DescribeProcessingJob\",\n",
    "                \"sagemaker:DescribeTrainingJob\",\n",
    "                \"sagemaker:DescribeTransformJob\",\n",
    "                \"sagemaker:ListProcessingJobs\",\n",
    "                \"sagemaker:ListTags\",\n",
    "                \"sagemaker:StopHyperParameterTuningJob\",\n",
    "                \"sagemaker:StopProcessingJob\",\n",
    "                \"sagemaker:StopTrainingJob\",\n",
    "                \"sagemaker:StopTransformJob\",\n",
    "                \"sagemaker:UpdateEndpoint\",\n",
    "                \"sns:Publish\",\n",
    "                \"sqs:SendMessage\"\n",
    "            ],\n",
    "            \"Resource\": \"*\"\n",
    "        }\n",
    "    ]\n",
    "}\n",
    "```\n",
    "\n",
    "3. Replace **NOTEBOOK_ROLE_ARN** in the above JSON with the ARN for your notebook that you created in the previous step.\n",
    "4. Choose **Review policy** and give the policy a name such as `StepFunctionsWorkflowExecutionPolicy`.\n",
    "5. Choose **Create policy**.\n",
    "6. Select **Roles** and search for your `StepFunctionsWorkflowExecutionRole` role.\n",
    "7. Under the **Permissions** tab, click **Attach policies**.\n",
    "8. Search for your newly created `StepFunctionsWorkflowExecutionPolicy` policy and select the check box next to it.\n",
    "9. Choose **Attach policy**. You will then be redirected to the details page for the role.\n",
    "10. Copy the StepFunctionsWorkflowExecutionRole **Role ARN** at the top of the Summary.\n",
    "\n",
    "<br/>\n",
    "<br/>\n",
    "<br/>\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Adjust the following line so that it references the StepFunctionsWorkflowExecutionRole ARN from above\n",
    "workflow_execution_role = \"YOUR_STEPFUNCTIONS_EXECUTION_ROLE_ARN_HERE\" "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create StepFunctions Workflow execution Input schema"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Generate unique names for Pre-Processing Job, Training Job, and Model Evaluation Job for the Step Functions Workflow\n",
    "temp_hex = uuid.uuid1().hex[:10]\n",
    "\n",
    "preprocessing_job_name = \"scikit-learn-sm-preprocessing-{}-{}\".format(\n",
    "    job_label, temp_hex\n",
    ")  # Each Preprocessing job requires a unique name,\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# SageMaker expects unique names for each job, model, and endpoint.\n",
    "# If these names are not unique the job execution (and associated Workflow) will fail.\n",
    "# In order to ensure unique job names, we will pass in these values dynamically for each \n",
    "# Worklow execution using the following placeholders:\n",
    "execution_input = ExecutionInput(\n",
    "    schema={\n",
    "        \"PreprocessingJobName\": str,\n",
    "    }\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Data pre-processing and feature engineering"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before introducing the script you use for data cleaning, pre-processing, and feature engineering, inspect the first 5 rows of the dataset. The target is predicting the `income` category. The features from the dataset you select are `age`, `education`, `major industry code`, `class of worker`, `num persons worked for employer`, `capital gains`, `capital losses`, and `dividends from stocks`.\n",
    "\n",
    "Again, this information is provided for the sake of completeness, but is not the focus of this workshop."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a pandas DataFrame from the input data, which resides in S3\n",
    "import pandas as pd\n",
    "\n",
    "input_data = \"s3://sagemaker-sample-data-{}/processing/census/census-income.csv\".format(region)\n",
    "df = pd.read_csv(input_data, nrows=10)\n",
    "df.head(n=5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<br/>\n",
    "<br/>\n",
    "\n",
    "In order to run our Scikit-Learn preprocessing script as a SageMaker Processing job, first create a `SKLearnProcessor`, which lets you run custom Scikit-Learn scripts within the SageMaker-provided sklearn container."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sklearn_processor = SKLearnProcessor(\n",
    "    framework_version=\"0.20.0\",\n",
    "    role=role,\n",
    "    instance_type=\"ml.m5.xlarge\",\n",
    "    instance_count=1,\n",
    "    max_runtime_in_seconds=1200,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following notebook cell writes a file `preprocessing.py`, which contains the Scikit-Learn pre-processing script. You can update the script, and rerun this cell to overwrite `preprocessing.py`. At a high level, the pre-processing script performs the following tasks:\n",
    "\n",
    "* Removes duplicates and rows with conflicting data\n",
    "* transforms the target `income` column into a column containing two labels.\n",
    "* transforms the `age` and `num persons worked for employer` numerical columns into categorical features by binning them\n",
    "* scales the continuous `capital gains`, `capital losses`, and `dividends from stocks` so they're suitable for training\n",
    "* encodes the `education`, `major industry code`, `class of worker` so they're suitable for training\n",
    "* splits the data into training and test datasets, and saves the training features and labels and test features and labels.\n",
    "\n",
    "In subsequent steps, our training script will use the pre-processed training features and labels to train a model, and our model evaluation script will later use the trained model and pre-processed test features and labels to evaluate the model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile preprocessing.py\n",
    "\n",
    "import argparse\n",
    "import os\n",
    "import warnings\n",
    "\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "from sklearn.model_selection import train_test_split\n",
    "from sklearn.preprocessing import StandardScaler, OneHotEncoder, LabelBinarizer, KBinsDiscretizer\n",
    "from sklearn.preprocessing import PolynomialFeatures\n",
    "from sklearn.compose import make_column_transformer\n",
    "\n",
    "from sklearn.exceptions import DataConversionWarning\n",
    "warnings.filterwarnings(action=\"ignore\", category=DataConversionWarning)\n",
    "\n",
    "columns = [\n",
    "    \"age\",\n",
    "    \"education\",\n",
    "    \"major industry code\",\n",
    "    \"class of worker\",\n",
    "    \"num persons worked for employer\",\n",
    "    \"capital gains\",\n",
    "    \"capital losses\",\n",
    "    \"dividends from stocks\",\n",
    "    \"income\",\n",
    "]\n",
    "class_labels = [\" - 50000.\", \" 50000+.\"]\n",
    "\n",
    "def print_shape(df):\n",
    "    negative_examples, positive_examples = np.bincount(df[\"income\"])\n",
    "    print(\n",
    "        \"Data shape: {}, {} positive examples, {} negative examples\".format(\n",
    "            df.shape, positive_examples, negative_examples\n",
    "        )\n",
    "    )\n",
    "\n",
    "if __name__ == \"__main__\":\n",
    "    parser = argparse.ArgumentParser()\n",
    "    parser.add_argument(\"--train-test-split-ratio\", type=float, default=0.3)\n",
    "    args, _ = parser.parse_known_args()\n",
    "\n",
    "    print(\"Received arguments {}\".format(args))\n",
    "\n",
    "    input_data_path = os.path.join(\"/opt/ml/processing/input\", \"census-income.csv\")\n",
    "\n",
    "    print(\"Reading input data from {}\".format(input_data_path))\n",
    "    df = pd.read_csv(input_data_path)\n",
    "    df = pd.DataFrame(data=df, columns=columns)\n",
    "    df.dropna(inplace=True)\n",
    "    df.drop_duplicates(inplace=True)\n",
    "    df.replace(class_labels, [0, 1], inplace=True)\n",
    "\n",
    "    negative_examples, positive_examples = np.bincount(df[\"income\"])\n",
    "    print(\n",
    "        \"Data after cleaning: {}, {} positive examples, {} negative examples\".format(\n",
    "            df.shape, positive_examples, negative_examples\n",
    "        )\n",
    "    )\n",
    "\n",
    "    split_ratio = args.train_test_split_ratio\n",
    "    print(\"Splitting data into train and test sets with ratio {}\".format(split_ratio))\n",
    "    X_train, X_test, y_train, y_test = train_test_split(\n",
    "        df.drop(\"income\", axis=1), df[\"income\"], test_size=split_ratio, random_state=0\n",
    "    )\n",
    "\n",
    "    preprocess = make_column_transformer(\n",
    "        (\n",
    "            [\"age\", \"num persons worked for employer\"],\n",
    "            KBinsDiscretizer(encode=\"onehot-dense\", n_bins=10),\n",
    "        ),\n",
    "        (\n",
    "            [\"capital gains\", \"capital losses\", \"dividends from stocks\"],\n",
    "            StandardScaler(),\n",
    "        ),\n",
    "        (\n",
    "            [\"education\", \"major industry code\", \"class of worker\"],\n",
    "            OneHotEncoder(sparse=False),\n",
    "        ),\n",
    "    )\n",
    "    print(\"Running preprocessing and feature engineering transformations\")\n",
    "    train_features = preprocess.fit_transform(X_train)\n",
    "    test_features = preprocess.transform(X_test)\n",
    "\n",
    "    print(\"Train data shape after preprocessing: {}\".format(train_features.shape))\n",
    "    print(\"Test data shape after preprocessing: {}\".format(test_features.shape))\n",
    "\n",
    "    train_features_output_path = os.path.join(\n",
    "        \"/opt/ml/processing/train\", \"train_features.csv\"\n",
    "    )\n",
    "    train_labels_output_path = os.path.join(\n",
    "        \"/opt/ml/processing/train\", \"train_labels.csv\"\n",
    "    )\n",
    "\n",
    "    test_features_output_path = os.path.join(\n",
    "        \"/opt/ml/processing/test\", \"test_features.csv\"\n",
    "    )\n",
    "    test_labels_output_path = os.path.join(\"/opt/ml/processing/test\", \"test_labels.csv\")\n",
    "\n",
    "    print(\"Saving training features to {}\".format(train_features_output_path))\n",
    "    pd.DataFrame(train_features).to_csv(\n",
    "        train_features_output_path, header=False, index=False\n",
    "    )\n",
    "\n",
    "    print(\"Saving test features to {}\".format(test_features_output_path))\n",
    "    pd.DataFrame(test_features).to_csv(\n",
    "        test_features_output_path, header=False, index=False\n",
    "    )\n",
    "\n",
    "    print(\"Saving training labels to {}\".format(train_labels_output_path))\n",
    "    y_train.to_csv(train_labels_output_path, header=False, index=False)\n",
    "\n",
    "    print(\"Saving test labels to {}\".format(test_labels_output_path))\n",
    "    y_test.to_csv(test_labels_output_path, header=False, index=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<br/>\n",
    "\n",
    "Upload the pre processing script to S3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "PREPROCESSING_SCRIPT_LOCATION = \"preprocessing.py\"\n",
    "\n",
    "input_code = sagemaker_session.upload_data(\n",
    "    PREPROCESSING_SCRIPT_LOCATION,\n",
    "    bucket=sagemaker_session.default_bucket(),\n",
    "    key_prefix=os.path.join(job_label, \"data/sklearn_processing/code\"),\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<br/>\n",
    "\n",
    "Define the S3 Locations for the processing output and training data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s3_bucket_base_uri = \"{}{}\".format(\"s3://\", sagemaker_session.default_bucket())\n",
    "output_data = \"{}/{}\".format(s3_bucket_base_uri, os.path.join(job_label, \"data/sklearn_processing/output\"))\n",
    "preprocessed_training_data = \"{}/{}\".format(output_data, \"train_data\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"s3_bucket_base_uri:\\n  \", s3_bucket_base_uri)\n",
    "print(\"output_data:\\n  \", output_data)\n",
    "print(\"preprocessed_training_data:\\n  \", preprocessed_training_data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<br/>\n",
    "\n",
    "### Create the State Machine Steps\n",
    "\n",
    "We will now create the [ProcessingStep](https://aws-step-functions-data-science-sdk.readthedocs.io/en/stable/sagemaker.html#stepfunctions.steps.sagemaker.ProcessingStep) which is the step within our State Machine that will launch a SageMaker Processing Job.\n",
    "\n",
    "This step will use the SKLearnProcessor as defined in the previous steps along with the inputs and outputs objects that are defined below."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Create [ProcessingInputs](https://sagemaker.readthedocs.io/en/stable/api/training/processing.html#sagemaker.processing.ProcessingInput) and [ProcessingOutputs](https://sagemaker.readthedocs.io/en/stable/api/training/processing.html#sagemaker.processing.ProcessingOutput)  objects for Inputs and Outputs respectively for the SageMaker Processing Job. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "inputs = [\n",
    "    ProcessingInput(\n",
    "        source=input_data,\n",
    "        destination=\"/opt/ml/processing/input\",\n",
    "        input_name=\"input-1\"\n",
    "    ),\n",
    "    ProcessingInput(\n",
    "        source=input_code,\n",
    "        destination=\"/opt/ml/processing/input/code\",\n",
    "        input_name=\"code\",\n",
    "    ),\n",
    "]\n",
    "\n",
    "outputs = [\n",
    "    ProcessingOutput(\n",
    "        source=\"/opt/ml/processing/train\",\n",
    "        destination=\"{}/{}\".format(output_data,\"train_data\"),\n",
    "        output_name=\"train_data\",\n",
    "    ),\n",
    "    ProcessingOutput(\n",
    "        source=\"/opt/ml/processing/test\",\n",
    "        destination=\"{}/{}\".format(output_data, \"test_data\"),\n",
    "        output_name=\"test_data\",\n",
    "    ),\n",
    "]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Create the `ProcessingStep`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# preprocessing_job_name = generate_job_name()\n",
    "processing_step = ProcessingStep(\n",
    "    \"SageMaker pre-processing step\",\n",
    "    processor=sklearn_processor,\n",
    "    job_name=execution_input[\"PreprocessingJobName\"],\n",
    "    inputs=inputs,\n",
    "    outputs=outputs,\n",
    "    container_arguments=[\"--train-test-split-ratio\", \"0.2\"],\n",
    "    container_entrypoint=[\"python3\", \"/opt/ml/processing/input/code/preprocessing.py\"],\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " <br/>\n",
    " \n",
    " #### Create a `Fail` state, which will capture any workflow failures"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "failed_state_sagemaker_processing_failure = stepfunctions.steps.states.Fail(\n",
    "    \"ML workflow failed\", cause=\"SageMakerProcessingJobFailed\"\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<br/>\n",
    "\n",
    "#### Add the `Error handling` in the workflow\n",
    "We will use the [Catch Block](https://aws-step-functions-data-science-sdk.readthedocs.io/en/stable/states.html#stepfunctions.steps.states.Catch) to perform error handling. If the Processing Job Step fails, the flow will go into the failure state.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "catch_state_processing = stepfunctions.steps.states.Catch(\n",
    "    error_equals=[\"States.TaskFailed\"],\n",
    "    next_step=failed_state_sagemaker_processing_failure,\n",
    ")\n",
    "\n",
    "processing_step.add_catch(catch_state_processing)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<br/>\n",
    "\n",
    "#### Create a `Success` state"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "success_state = stepfunctions.steps.states.Succeed(\"ML workflow succeeded\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<br/>\n",
    "\n",
    "## Create the `Workflow` "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "workflow_name = \"SageMakerMLopsWorkflow3-%s-%d\" % (job_label, time.time())\n",
    "print(\"workflow_name:\",workflow_name)\n",
    "\n",
    "workflow_graph = Chain([processing_step, success_state])\n",
    "\n",
    "branching_workflow = Workflow(\n",
    "    name=workflow_name,\n",
    "    definition=workflow_graph,\n",
    "    role=workflow_execution_role,\n",
    ")\n",
    "\n",
    "branching_workflow.create()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<br/>\n",
    "\n",
    "## View the generated State Machine definition\n",
    "\n",
    "The Step Functions Data Science SDK automatically generates an AWS Step Functions State Machine definition based on the various Steps and Workflow, defined above. It can be helpful to examine the resulting Step Functions definition, as we'll do here:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "import json\n",
    "\n",
    "j = json.loads(branching_workflow.definition.to_json())\n",
    "print(json.dumps(j, indent=2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<br/>\n",
    "\n",
    "## Manually execute the Workflow\n",
    "\n",
    "<br/>\n",
    "\n",
    "First, start a State Machine execution:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "execution = branching_workflow.execute(\n",
    "    inputs={\n",
    "        \"PreprocessingJobName\": preprocessing_job_name,  # Each pre-processing job requires a unique name\n",
    "    }\n",
    ")\n",
    "\n",
    "execution_output = execution.get_output(wait=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<br/>\n",
    "\n",
    "Next, monitor the running State Machine execution. Run the following cell, and then click the resulting URL in order to view the execution status:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "execution"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<br/>\n",
    "\n",
    "## How can we query the detailed status for any given State Machine execution?\n",
    "### First, we will need the workflow execution ARN:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "execution.execution_arn"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Next, we can use the boto3 library to describe the above workflow execution:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sfn_client = boto3.client('stepfunctions')\n",
    "desc = sfn_client.describe_execution(executionArn=execution.execution_arn)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "for key in desc.keys():\n",
    "    print(\"%s:\\n   %s\\n\" % (key, desc[key]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### And, we can also retrieve the detailed Workflow execution history"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hist = sfn_client.get_execution_history(\n",
    "    executionArn=execution.execution_arn, \n",
    "    includeExecutionData=False,\n",
    "    maxResults = 1000)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hist.keys()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df = pd.DataFrame(hist['events'])\n",
    "df.head(30)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<br/>\n",
    "<br/>\n",
    "\n",
    "## Adding scheduled Workflow execution\n",
    "\n",
    "The next step is to configure scheduled Workflow execution. To accomplish this, we will be using Amazon EventBridge. If you are not familiar with EventBridge, it is a serverless event bus that makes it easier to build event-driven applications at scale using events generated from your applications, integrated Software-as-a-Service (SaaS) applications, and AWS services. You can find more details in [the documentation](https://docs.aws.amazon.com/eventbridge/latest/userguide/eb-what-is.html).\n",
    "\n",
    "Before we can create the scheduled rule, we first need to create a Lambda function that will be used to trigger our Workflow execution\n",
    "\n",
    "### Create Lambda function\n",
    "\n",
    "* Navigate to the `AWS Lambda console`, and choose `Create function`\n",
    "* Select `Author from scratch`\n",
    "* For `Function name`, enter `mlops_trigger_workflow_YOUR_UNIQUE_ID`\n",
    "* Under `Runtime`, choose `Python 3.8`\n",
    "* Click `Create function`\n",
    "* Click `Permissions` and under `Execution role`, click the link next to your Role name. This will bring you to the IAM console\n",
    "* Within your Lambda role view (in the IAM console) click `Add inline policy`\n",
    "* For `Service`, choose `Step Functions`\n",
    "* Under `Actions`, choose `Write` -> `Start Execution`\n",
    "* Under `Resources`, specify your unique Step Functions ARN\n",
    "* Click `Review Policy`\n",
    "* For `Name`, enter `lambda__sfn_execute_workflow_YOUR_UNIQUE_ID`\n",
    "* Click `Create policy`\n",
    "    * Your new inline policy will now be associated with your Lambda execution role\n",
    "* Return to the `AWS Lambda console`\n",
    "* Under `Code`, open `lambda_function.py` within the code editor, and paste in the following python code:\n",
    "\n",
    "```python\n",
    "import json\n",
    "import boto3\n",
    "import uuid\n",
    "import os\n",
    "\n",
    "def lambda_handler(event, context):\n",
    "    client = boto3.client(\"stepfunctions\")\n",
    "    tmphex = uuid.uuid1().hex[:8]\n",
    "    \n",
    "    # Retrieve configuration from Lambda environment variables\n",
    "    job_label = os.environ.get('JOB_LABEL', \"Error! No JOB_LABEL defined!\")\n",
    "    state_machine_arn = os.environ.get('STATE_MACHINE_ARN', \"Error! No STATE_MACHINE_ARN defined!\")\n",
    "    execution_prefix = os.environ.get('EXECUTION_PREFIX', \"Error! No EXECUTION_PREFIX defined!\")\n",
    "    \n",
    "    print(\"job_label:\", job_label)\n",
    "    print(\"state_machine_arn:\", state_machine_arn)\n",
    "    print(\"execution_prefix:\", execution_prefix)\n",
    "    \n",
    "    # Define state machine workflow inputs\n",
    "    inputs = { \"PreprocessingJobName\": \"scikit-learn-sm-preprocessing-{}-{}\".format(job_label, tmphex) }\n",
    "    \n",
    "    response = client.start_execution(\n",
    "        stateMachineArn=state_machine_arn,\n",
    "        name=\"{}_{}\".format(execution_prefix, tmphex),\n",
    "        input=json.dumps(inputs)\n",
    "    )\n",
    "\n",
    "    return { \"executionArn\": response['executionArn'],\n",
    "        \"ResponseMetadata\": response['ResponseMetadata'],\n",
    "        \"workflowInputs\": inputs\n",
    "    }\n",
    "\n",
    "```\n",
    "\n",
    "<br/>\n",
    "\n",
    "* Click `Configuration` and scroll down to `Environment variables`, and create the following 3 variables:\n",
    "    * Key `EXECUTION_PREFIX` with value `mlops_workflow_execution`\n",
    "    * Key `JOB_LABEL` with value `YOUR_UNIQUE_ID`\n",
    "    * Key `STATE_MACHINE_ARN` with value `YOUR_STATE_MACHINE_ARN`\n",
    "* Scroll up to the code editor, and choose `Deploy`\n",
    "* At the top of the Lambda console, configure a Test (the input variables do not matter)\n",
    "* Try executing your test. If everything has been configured properly, the test should trigger your State Machine to begin an execution\n",
    "    * Confirm whether or not the State Machine is executing, by visiting the `Step Functions console`\n",
    "    \n",
    "### Create EventBridge schedule\n",
    "* Navigate to the `Amazon EventBridge console` and look for `Rules`\n",
    "* Ensure that the default event bus is selected\n",
    "* Create a new rule named `mlops_schedule_YOUR_UNIQUE_ID`\n",
    "* Under `Define pattern`, choose `Schedule`\n",
    "* Choose an appropriate schedule (ex: every 15 minutes)\n",
    "* Under `Target`, select your newly created Lambda function\n",
    "* Click `Create`\n",
    "\n",
    "At this point, your Workflow should now execute on the schedule that you specified. Wait for your Workflow to being executing (confirm within the Step Functions console). If the Workflow does not execute as expected, go back and review your Lambda and EventBridge schedule configurations. If you need a hand in troubleshooting any issues, please flag down one of the AWS SAs.\n",
    "\n",
    "Once you have confirmed that the Workflow is executing on the proper schedule, please remove your EventBridge scheduled rule (in order to avoid unnecessary costs)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<br/>\n",
    "<br/>\n",
    "\n",
    "## Clean Up\n",
    "When you are done, make sure to clean up your AWS account by deleting resources you won't be reusing. Uncomment the code below and run the cell to delete the State Machine. Also make sure that you manually clean-up your SNS Topic/Subscription, Lambda function, and EventBridge rules, and remove any unwanted files/folders from S3."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#branching_workflow.delete()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "conda_python3",
   "language": "python",
   "name": "conda_python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
