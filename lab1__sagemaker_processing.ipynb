{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introductory SageMaker Processing Example\n",
    "\n",
    "This notebook corresponds to the section \"Preprocessing Data With The Built-In Scikit-Learn Container\" in a [related SageMaker blog post](https://aws.amazon.com/blogs/aws/amazon-sagemaker-processing-fully-managed-data-processing-and-model-evaluation/). \n",
    "\n",
    "The notebook shows a basic example of using SageMaker Processing to pre-process a CSV-based dataset using the popular Scikit-Learn library in order to create train/test/validation sets. Although this example is relatively trivial, a similar workflow can be used to perform complex data pre-processing tasks by modifying the notebook to include your own data pre-processing code (ex: preprocessing.py, below). Also keep in mind that there is no requirement to use Scikit-Learn in your own data processing jobs. Alternative frameworks such as Spark and Dask can also be used. You are also free to leverage your own custom Docker container, containing the specific combination of software/libraries that suits your needs.\n",
    "\n",
    "## SageMaker Processing - High-level Architecture\n",
    "A SageMaker Processing job typically downloads input data from Amazon Simple Storage Service (Amazon S3), processes the data, and then uploads the results back into S3.\n",
    "\n",
    "<img src='images/Processing-1.jpg'/>\n",
    "\n",
    "## Hands-on Lab\n",
    "\n",
    "First, let’s create an SKLearnProcessor object from the sagemaker SDK, passing the Scikit-Learn version we want to use, as well as our managed infrastructure requirements."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import boto3\n",
    "import sagemaker\n",
    "from sagemaker import get_execution_role\n",
    "from sagemaker.sklearn.processing import SKLearnProcessor\n",
    "\n",
    "region = boto3.session.Session().region_name  # determine current AWS region\n",
    "role = get_execution_role()  # determine IAM role associated with this notebook instance\n",
    "\n",
    "sklearn_processor = SKLearnProcessor(\n",
    "    framework_version=\"0.20.0\", role=role, instance_type=\"ml.m5.xlarge\", instance_count=1\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<br/>\n",
    "\n",
    "Next, let's read in the raw dataset from a public S3 bucket, and save a copy to our local notebook filesystem"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "\n",
    "input_data = \"s3://sagemaker-sample-data-{}/processing/census/census-income.csv\".format(region)\n",
    "df = pd.read_csv(input_data)\n",
    "df.to_csv(\"dataset.csv\")\n",
    "df.head(5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<br/>\n",
    "\n",
    "In this next cell, we write out the data processing script (preprocessing.py) that will be used by SageMaker Processing. Although we are making a local copy of the processing script here, the actual Processing job will run within SageMaker-managed infrastructure that is created when we launch the job in a subsequent step."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile preprocessing.py\n",
    "import pandas as pd\n",
    "import os\n",
    "from sklearn.model_selection import train_test_split\n",
    "\n",
    "# Read data locally\n",
    "input_data_path = os.path.join(\"/opt/ml/processing/input\", \"dataset.csv\")\n",
    "\n",
    "# Preprocess the data set\n",
    "df = pd.read_csv(input_data_path)\n",
    "downsampled = df.sample(50000)\n",
    "print(\"Shape of data is:\")\n",
    "print(downsampled.shape)\n",
    "\n",
    "# Split data set into training, validation, and test\n",
    "train, test = train_test_split(downsampled, test_size=0.2)\n",
    "train, validation = train_test_split(train, test_size=0.2)\n",
    "\n",
    "# Create local output directories, as required\n",
    "try:\n",
    "    output_dirs = [\"/opt/ml/processing/output/train\",\n",
    "             \"/opt/ml/processing/output/validation\",\n",
    "             \"/opt/ml/processing/output/test\"]\n",
    "    for d in output_dirs:\n",
    "        if not os.path.exists(d):\n",
    "            os.mkdir(d)\n",
    "except Exception as e:\n",
    "    # if the Processing call already creates these directories (or directory otherwise cannot be created)\n",
    "    print(e)\n",
    "    print(\"Could Not Make Directories\")\n",
    "    pass\n",
    "\n",
    "# Save data locally\n",
    "try:\n",
    "    train.to_csv(\"/opt/ml/processing/output/train/train.csv\")\n",
    "    validation.to_csv(\"/opt/ml/processing/output/validation/validation.csv\")\n",
    "    test.to_csv(\"/opt/ml/processing/output/test/test.csv\")\n",
    "    print(\"Files Successfully Written\")\n",
    "except Exception as e:\n",
    "    print(\"Could Not Write the Files\")\n",
    "    print(e)\n",
    "    pass\n",
    "\n",
    "print(\"Finished running processing job\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<br/>\n",
    "\n",
    "With our dataset and processing script defined, we can now launch the actual SageMaker Processing job. Note the `inputs` and `outputs` specified below. These 2 parameters are used to inform the Processing job where it will find its input data (inputs), as well as where it will find the processed data within the associated container (outputs). Remember, SageMaker Processing will copy the `outputs` back to S3 once the Processing job has completed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sagemaker.processing import ProcessingInput, ProcessingOutput\n",
    "\n",
    "sklearn_processor.run(\n",
    "    code=\"preprocessing.py\",\n",
    "    # arguments = ['arg1', 'arg2'],\n",
    "    inputs=[ProcessingInput(source=\"dataset.csv\", destination=\"/opt/ml/processing/input\")],\n",
    "    outputs=[\n",
    "        ProcessingOutput(source=\"/opt/ml/processing/output/train\"),\n",
    "        ProcessingOutput(source=\"/opt/ml/processing/output/validation\"),\n",
    "        ProcessingOutput(source=\"/opt/ml/processing/output/test\"),\n",
    "    ],\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<br/>\n",
    "\n",
    "Once the job has completed, let's examine the job details:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sklearn_processor.jobs[0].describe()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<br/>\n",
    "\n",
    "Next, let's retrieve one of the newly created output files from S3. Here, we will retrieve the test dataset `test.csv`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "test_csv_path = sklearn_processor.jobs[0].describe()['ProcessingOutputConfig']['Outputs'][2]['S3Output']['S3Uri'] + \"/test.csv\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "test_csv_path"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "test_df = pd.read_csv(test_csv_path)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Test dataset has %d rows\" % len(test_df))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "test_df.head(5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "conda_python3",
   "language": "python",
   "name": "conda_python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
